    //panggil template header
    //panggil class yang berisi fungsi dml
    //buat judul tabel
    //buat objek dimana dia akan memanggil fungsi dari class yang sudah dimasukkan
    //buat header untuk tampilan table
    //lakukan looping untuk menampilkan seluruh isi data table
    //tambahkan button untuk menambah data
    //panggil template footer
<?php
include_once 'top.php';
require_once 'db/class_peserta.php';
?>
<h2>Daftar Peserta</h2>
<?php
$obj = new Peserta();
$rows = $obj->getAll();
?>
<table class="table">
    <thead>
    <tr class="active">
        <th>No</th><th>NO Registrasi</th><th>Nama Lengkap</th>
        <th>Email</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['nomor'].'</td>';
        echo '<td>'.$row['namalengkap'].'</td>';
        echo '<td>'.$row['email'].'</td>';
        echo '<td><a href="view_peserta.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_peserta.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>
<?php
include_once 'bottom.php';
?>
