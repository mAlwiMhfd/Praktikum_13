<?php
    //panggil template header
    //buat form untuk memasukkan input dimana saat melakukan input pertama kali, form akan kosong dan saat melakukan update, form sudah terisi dengan data yang akan diupdate
    //buat fungsi dimana button simpan hanya terlihat saat melakukan penambahan ke db dan fungsi update, delete hanya tampil saat melakukan update
    //panggil template footer
?>
<?php
    include_once 'top.php';
    require_once 'proses_peserta.php';
    $obj_peserta = new Peserta();
    $_idedit = $_GET['id'];

    if(!empty($_idedit)){
        $data = $obj_peserta->findByID($_idedit);
    }else{
        $data = [] ; // array kosong data baru
    }

?>

<form class="form-horizontal" method="POST" action="proses_peserta.php">
  <fieldset>
    <!-- Form Name -->
    <legend>Form Entry Peserta</legend>
    <!-- Text input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="nomor">nomor</label>
          <div class="col-md-2">
             <input id="nomor" name="nomor" placeholder="nomor" class="form-control input-md" type="text"  value="<?php echo $data['nomor']?>">
     </div>
   </div>

   <!-- Text input-->
   <div class="form-group">
      <label class="col-md-4 control-label" for="namalengkap">nama lengkap</label>
        <div class="col-md-5">
          <input id="namalengkap" name="namalengkap" placeholder="nama lengkap" class="form-control input-md" type="text"  value="<?php echo $data['namalengkap']?>">
        </div>
     </div>

   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label"
      for="email">email</label>
         <div class="col-md-4">
           <input id="email" name="email" placeholder="email" class="form-control input-md" type="text" value="<?php echo $data['email']?>">
     </div>
   </div>

   <div class="form-group">
     <label class="col-md-4 control-label" for="kegiatan_id">kegiatan_id</label>
       <div class="col-md-4">
         <select id="kegiatan_id" name="kegiatan_id" class="form-control">
           <option value="1">1</option>
           <option value="2">2</option>
           <option value="3">3</option>
         </select>
     </div>
   </div>

   <div class="form-group">
     <label class="col-md-4 control-label" for="jenis_id">jenis_id</label>
       <div class="col-md-4">
         <select id="jenis_id" name="jenis_id" class="form-control">
           <option value="1">1</option>
           <option value="2">2</option>
           <option value="3">3</option>
           <option value="4">4</option>
         </select>
     </div>
   </div>







   <!-- Button (Double) -->
     <!-- <div class="form-group">
       <label class="col-md-4 control-label" for="proses"></label>
         <div class="col-md-8">
           <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
           <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
           <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
       </div>
     </div>

   </fieldset> -->

     <?php
      if (empty($_idedit)){
       ?>
       <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
       <?php
      } else {
         ?>
         <input type="hidden" name="idedit" value="<?php echo $_idedit?>" />
         <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
         <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
      <?php
       } ?>
  </form>
<?php
    include_once 'bottom.php';
?>
