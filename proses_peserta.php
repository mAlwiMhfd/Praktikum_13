<?php
require_once 'db/class_peserta.php';
$objpeserta = new Peserta();

// tangkap request POST
$_nomor = $_POST['nomor'];
$_email = $_POST['email'];
$_namalengkap = $_POST['namalengkap'];
$_kegiatan_id = $_POST['kegiatan_id'];
$_jenis_id = $_POST['jenis_id'];

$_proses = $_POST['proses'];

// buat array data
$ar_data[] = $_nomor; // ? ke-1 nomor
$ar_data[] = $_email;// ? ke-2 email
$ar_data[] = $_namalengkap;// ? ke-3 namalengkap
$ar_data[] = $_kegiatan_id;
$ar_data[] = $_jenis_id;
// logik simpan , update atau hapus

$row = 0 ; // baris eksekusi
if($_proses == "Simpan"){
  $row = $objpeserta->simpan($ar_data);
}elseif ($_proses=="Update"){
  $_idedit = $_POST['idedit'];// tangkap idedit
  $ar_data[]= $_idedit;// ? ke-6 id (WHERE ID=?)
  $row = $objpeserta->ubah($ar_data);
}elseif ($_proses=="Hapus"){
unset($ar_data);//hapus array
  $_idedit = $_POST['idedit'];// tangkap idedit
  $row = $objpeserta->hapus($_idedit);
}
if ($row==0){
  //echo "GAGAL PROSES !!";
}else {
//echo "PROSES SUKSES !!";
// redirect page ke index.php
header('Location:index.php');
}
?>
