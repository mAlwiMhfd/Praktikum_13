<?php
    //masukkan deskripsi tabel agar mudah membuat fungsi. gunakan comment utk mempermudah
    //panggil file yang berisi semua fungsi dml yang bisa diakses oleh semua class
    //buat class dimana class ini merupakan turunan dari class yang dipanggil diatas
        //buat fungsi untuk bisa mengambil semua fungsi dari class parentnya
        //buat fungsi dml yang hanya bisa digunakan di class ini
    //tutup class
    /*
    MariaDB [dbkegiatan]> desc peserta;
    +-------------+-------------+------+-----+---------+----------------+
    | Field       | Type        | Null | Key | Default | Extra          |
    +-------------+-------------+------+-----+---------+----------------+
    | id          | int(11)     | NO   | PRI | NULL    | auto_increment |
    | nomor       | varchar(15) | YES  | UNI | NULL    |                |
    | email       | varchar(45) | YES  |     | NULL    |                |
    | namalengkap | varchar(45) | YES  |     | NULL    |                |
    | hp          | varchar(30) | YES  |     | NULL    |                |
    | fbaccount   | varchar(30) | YES  |     | NULL    |                |
    | kegiatan_id | int(11)     | NO   | MUL | NULL    |                |
    | status      | varchar(10) | YES  |     | NULL    |                |
    | jenis_id    | int(11)     | NO   | MUL | NULL    |                |
    | tgl_daftar  | date        | YES  |     | NULL    |                |
    +-------------+-------------+------+-----+---------+----------------+
    */
    require_once "class_DAO.php";
    class Peserta extends DAO
    {
        public function __construct()
        {
            parent::__construct("peserta");
        }
    
        public function simpan($data){
            $sql = "INSERT INTO " . $this->tableName ."SET id=?, nomor=?, email=?, namalengkap=?, fbaccount=?, kegiatan_id, status=?, jenis_id=?, tgl_daftar=?";
            $ps = $this->koneksi->prepare(sql);
            $ps->execute($data);
            return $ps->rowCount();
         }
    
        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET nomor=?,email=?,namalengkap=?,kegiatan_id=?,jenis_id=? ".
            " WHERE id=?";

            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
           return $ps->rowCount();
        }
    }
    
    
    ?>