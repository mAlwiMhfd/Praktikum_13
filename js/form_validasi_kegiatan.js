$(function() {
    // Initialize form validation on the form, It has the name attribute ""
    $("form[name='form_kegiatan']").validate({
    rules: {
    kode: {
    required:true,
    maxlength:10,
    },
    judul: "required",
    narasumber: "required",
    deskripsi: "required",
    },
    // Specify validation error messages
    messages: {
    kode: {
    required : "Kode wajib di isi !!",
    maxlength : "maksimum 10 character !!"
    },
    judul: "Judul wajib diisi !!",
    narasumber: "Narasumber wajib diisi !!",
    deskripsi:"Deskripsi wajib diisi",
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
    form.submit();
    }
    });
   });